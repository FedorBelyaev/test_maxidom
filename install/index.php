<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Belyaev\Maxidom\BrandTable;
use Belyaev\Maxidom\CatalogTable;
use Bitrix\Main\Application;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Db\SqlQueryException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\SystemException;

Loc::loadMessages(__FILE__);

if (class_exists('belyaev_maxidom')) {
    return;
}

/**
 * Class belyaev_maxidom
 */
class belyaev_maxidom extends CModule
{
    /** @var string */
    public $MODULE_ID = 'belyaev.maxidom';

    /** @var string */
    public $MODULE_VERSION;

    /** @var string */
    public $MODULE_VERSION_DATE;

    /** @var string */
    public $MODULE_NAME;

    /** @var string */
    public $MODULE_DESCRIPTION;

    /** @var string */
    public $MODULE_GROUP_RIGHTS;

    /** @var string */
    public $PARTNER_NAME = '';

    /** @var string */
    public $PARTNER_URI;

    /**
     * belyaev_maxidom constructor.
     */
    public function __construct()
    {
        $this->MODULE_ID = 'belyaev.maxidom';
        $arModuleVersion = array();
        include(__DIR__."/version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = Loc::getMessage('BELYAEV_MAXIDOM_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('BELYAEV_MAXIDOM_MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = "Belyaev";
        $this->PARTNER_URI = "http://www.quazarteam.ru";
    }

    /**
     * инсталляция модуля
     */
    public function doInstall()
    {
        // регистрируем модуль
        ModuleManager::registerModule($this->MODULE_ID);
        // инсталляция
        $this->InstallDB(); // инсталлируем БД
        $this->InstallEvents(); // инсталлируем события
        $this->installFiles(); // инсталляция файлов
    }

    /**
     * деинсталляция модуля
     */
    public function doUninstall()
    {
        // деинсталляция
        $this->UnInstallDB(); // деинсталлируем БД
        $this->UnInstallEvents(); // деинсталлируем события
        $this->UnInstallFiles(); // инсталляция файлов
        // удаляем модуль из Битрикс (unregister)
        ModuleManager::unregisterModule($this->MODULE_ID);
    }

    /**
     * инсталляция БД
     * @return bool|void
     * @throws ArgumentException
     * @throws LoaderException
     * @throws SystemException
     */
    function InstallDB()
    {
        if (Loader::includeModule($this->MODULE_ID))
        {
            BrandTable::getEntity()->createDbTable();
            CatalogTable::getEntity()->createDbTable();
        }
    }

    /**
     * деинсталляция БД
     * @throws SqlQueryException
     * @throws LoaderException
     * @throws SystemException
     */
    function UnInstallDB()
    {
        if (Loader::includeModule($this->MODULE_ID))
        {
            $connection = Application::getInstance()->getConnection();
            $connection->dropTable(CatalogTable::getTableName());
            $connection->dropTable(BrandTable::getTableName());
        }
    }


    /**
     * инсталляция событий
     * @return bool|void
     */
    function InstallEvents()
    {
        Bitrix\Main\EventManager::getInstance()->registerEventHandler(
            'sale',
            'OnSaleOrderSaved',
            $this->MODULE_ID,
            'Belyaev\Maxidom\Events',
            'OnSaleOrderSaved'
        );
        return true;
    }

    /**
     * деинсталляция событий
     * @return bool|void
     */
    function UnInstallEvents()
    {
        Bitrix\Main\EventManager::getInstance()->unRegisterEventHandler(
            'sale',
            'OnSaleOrderSaved',
            $this->MODULE_ID,
            'Belyaev\Maxidom\Events',
            'OnSaleOrderSaved'
        );
        return true;
    }



    /**
     * инстлляция файлов
     * @return bool|void
     */
    public function installFiles()
    {
        // инсталлируем компоненты
        CopyDirFiles(
            $this->GetPath()."/install/components",
            Bitrix\Main\Application::getDocumentRoot()."/local/components",
            true,
            true
        );
        // инсталлируем в корень
        CopyDirFiles(
            $this->GetPath()."/install/assets",
            Bitrix\Main\Application::getDocumentRoot()."/",
            true,
            true
        );
        return true;
    }

    /**
     * деинсталляция файлов
     * @return bool|void
     */
    function UnInstallFiles()
    {
        // деинсталлируем компонент /local/components/belyaev/tag.selector.ajax
        Bitrix\Main\IO\Directory::deleteDirectory(
            Bitrix\Main\Application::getDocumentRoot() . "/local/components/belyaev/brand.list"
        );
        // деинсталлируем компонент /local/components/belyaev/catalog.list
        Bitrix\Main\IO\Directory::deleteDirectory(
            Bitrix\Main\Application::getDocumentRoot() . "/local/components/belyaev/catalog.list"
        );
        // деинсталлируем /maxidom_brand
        Bitrix\Main\IO\Directory::deleteDirectory(
            Bitrix\Main\Application::getDocumentRoot() . "/maxidom_brand"
        );
        // деинсталлируем /test_soap.php
        Bitrix\Main\IO\Directory::deleteDirectory(
            Bitrix\Main\Application::getDocumentRoot() . "/test_soap.php"
        );
        // деинсталлируем /test_soap_create.php
        Bitrix\Main\IO\Directory::deleteDirectory(
            Bitrix\Main\Application::getDocumentRoot() . "/test_soap_create.php"
        );
        // деинсталлируем /test_soap_crud.php
        Bitrix\Main\IO\Directory::deleteDirectory(
            Bitrix\Main\Application::getDocumentRoot() . "/test_soap_crud.php"
        );
        return true;
    }

    /**
     * функция возвращает текущий PATH для инсталлятора
     * @param bool $notDocumentRoot
     * @return mixed|string
     */
    function GetPath($notDocumentRoot=false)
    {
        if($notDocumentRoot)
            return str_ireplace(Application::getDocumentRoot(),'',dirname(__DIR__));
        else
            return dirname(__DIR__);
    }

}

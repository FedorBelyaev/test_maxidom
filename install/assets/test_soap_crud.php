<?

use Bitrix\Main\Loader;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

echo "<pre>";
Loader::includeModule('belyaev.maxidom');

\Bitrix\Main\Loader::includeModule("webservice");
$client  = new CSOAPClient($_SERVER['SERVER_NAME'], '/test_soap.php');

echo "\ncreateBrand\n";
$request = new CSOAPRequest("createBrand", "maxidom.soap");
$request->addParameter("arParams", ["BRAND_ID" => 8, "SORT" => 100,]);
$response = $client->send($request);
$brandId = 0;
if ( $response->isFault() ) {
    echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
}
else {
    echo( "[OK]: ".print_r($response->Value, 1));
    $brandId = $response->Value["id"];
}


if($brandId){
    echo "\nreadBrand\n";
    $request = new CSOAPRequest("readBrand", "maxidom.soap");
    $request->addParameter("id", intval($brandId));
    $response = $client->send($request);
    if ( $response->isFault() ) {
        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
    }


    echo "\nupdateBrand\n";
    $request = new CSOAPRequest("updateBrand", "maxidom.soap");
    $request->addParameter("id", intval($brandId));
    $request->addParameter("brand", ["BRAND_ID" => 9, "SORT" => 200]);
    $response = $client->send($request);
    if ( $response->isFault() ) {
        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
    }



    echo "\ncreateCatalog\n";
    $request = new CSOAPRequest("createCatalog", "maxidom.soap");
    $request->addParameter("arParams",  ["BRAND_ID" => $brandId, "ELEMENT_ID" => 18, "SORT" => 100,]);
    $response = $client->send($request);

    if ( $response->isFault() ) {

        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
        $catalogId = $response->Value["id"];
    }

    echo "\nreadCatalog\n";
    $request = new CSOAPRequest("readCatalog", "maxidom.soap");
    $request->addParameter("id", intval($catalogId));
    $response = $client->send($request);
    if ( $response->isFault() ) {
        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
    }


    echo "\ncreateCatalog\n";
    $request = new CSOAPRequest("createCatalog", "maxidom.soap");
    $request->addParameter("arParams", ["BRAND_ID" => $brandId, "ELEMENT_ID" => 19, "SORT" => 100, ]);
    $response = $client->send($request);
    if ( $response->isFault() ) {
        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
        $catalogId = $response->Value["id"];
    }

    echo "\nreadCatalog\n";
    $request = new CSOAPRequest("readCatalog", "maxidom.soap");
    $request->addParameter("id", intval($catalogId));
    $response = $client->send($request);
    if ( $response->isFault() ) {
        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
    }

    echo "\ndeleteCatalog\n";
    $request = new CSOAPRequest("deleteCatalog", "maxidom.soap");
    $request->addParameter("id", intval($catalogId));
    $response = $client->send($request);
    if ( $response->isFault() ) {
        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
    }



    echo "\ncreateCatalog\n";
    $request = new CSOAPRequest("createCatalog", "maxidom.soap");
    $request->addParameter("arParams", ["BRAND_ID" => $brandId, "ELEMENT_ID" => 20, "SORT" => 100,]);
    $response = $client->send($request);
    if ( $response->isFault() ) {
        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
        $catalogId = $response->Value["id"];
    }

    echo "\nupdateCatalog\n";
    $request = new CSOAPRequest("updateCatalog", "maxidom.soap");
    $request->addParameter("arParams", ["SORT" => 300,]);
    $response = $client->send($request);
    if ( $response->isFault() ) {
        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
        $catalogId = $response->Value["id"];
    }

    echo "\nreadCatalog\n";
    $request = new CSOAPRequest("readCatalog", "maxidom.soap");
    $request->addParameter("id", intval($catalogId));
    $response = $client->send($request);
    if ( $response->isFault() ) {
        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
    }


}

echo "createBrand\n";
$request = new CSOAPRequest("createBrand", "maxidom.soap");
$request->addParameter("arParams", ["BRAND_ID" => 10, "SORT" => 100,]);
$response = $client->send($request);
$brandId1 = 0;
if ( $response->isFault() ) {
    echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
}
else {
    echo( "[OK]: ".print_r($response->Value, 1));
    $brandId1 = $response->Value["id"];
}

echo "\nreadBrand\n";
$request = new CSOAPRequest("readBrand", "maxidom.soap");
$request->addParameter("id", intval($brandId1));
$response = $client->send($request);
if ( $response->isFault() ) {
    echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
}
else {
    echo( "[OK]: ".print_r($response->Value, 1));
}

echo "\ndeleteBrand\n";
$request = new CSOAPRequest("deleteBrand", "maxidom.soap");
$request->addParameter("id", intval($brandId1));
$response = $client->send($request);
if ( $response->isFault() ) {
    echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
}
else {
    echo( "[OK]: ".print_r($response->Value, 1));
}


echo "</pre>";
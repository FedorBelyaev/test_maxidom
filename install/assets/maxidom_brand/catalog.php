<?php require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php';

$APPLICATION->SetTitle('Каталог бренда');
$APPLICATION->SetPageProperty('title', '');
$APPLICATION->SetPageProperty('description', '');

$APPLICATION->IncludeComponent("belyaev:catalog.list", '', [
    "IBLOCK_ID" => 2,
    "SECTION_URL" => "/maxidom_brand/element.php?BRAND_ID={$_REQUEST["BRAND_ID"]}",
    "DETAIL_URL" => "/maxidom_brand/element.php?ELEMENT_ID=#ID#&BRAND_ID={$_REQUEST["BRAND_ID"]}",
    "BASKET_URL" => "/maxidom_brand/basket.php",

]);

require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php';
<?

use Bitrix\Main\Loader;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

echo "<pre>";
Loader::includeModule('belyaev.maxidom');

\Bitrix\Main\Loader::includeModule("webservice");
$client  = new CSOAPClient($_SERVER['SERVER_NAME'], '/test_soap.php');

echo "createBrand\n";
$ap_param = [
//    "NAME" => "Adidas",
    "BRAND_ID" => 8,
    "SORT" => 100,
];
$request = new CSOAPRequest("createBrand", "maxidom.soap");
$request->addParameter("arParams", $ap_param);
$response = $client->send($request);
$brandId = 0;
if ( $response->isFault() ) {
    echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
}
else {
    echo( "[OK]: ".print_r($response->Value, 1));
    $brandId = $response->Value["id"];
}


if($brandId){
    echo "\nreadBrand\n";
    $request = new CSOAPRequest("readBrand", "maxidom.soap");
    $request->addParameter("id", intval($brandId));
    $response = $client->send($request);
    if ( $response->isFault() ) {
        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
//        $brandId = $response->Value["id"];
    }



    echo "createCatalog\n";
    $ap_param = [
        "BRAND_ID" => $brandId,
        "ELEMENT_ID" => 18,
        "SORT" => 100,
    ];
    $request = new CSOAPRequest("createCatalog", "maxidom.soap");
    $request->addParameter("arParams", $ap_param);
    $response = $client->send($request);

    if ( $response->isFault() ) {

        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );

    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
        $catalogId = $response->Value["id"];
    }

    echo "\nreadCatalog\n";
    $request = new CSOAPRequest("readCatalog", "maxidom.soap");
    $request->addParameter("id", intval($catalogId));
    $response = $client->send($request);
    if ( $response->isFault() ) {
        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
//        $brandId = $response->Value["id"];
    }


    echo "createCatalog\n";
    $ap_param = [
        "BRAND_ID" => $brandId,
        "ELEMENT_ID" => 19,
        "SORT" => 100,
    ];
    $request = new CSOAPRequest("createCatalog", "maxidom.soap");
    $request->addParameter("arParams", $ap_param);
    $response = $client->send($request);
    if ( $response->isFault() ) {
        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
        $catalogId = $response->Value["id"];
    }

    echo "\nreadCatalog\n";
    $request = new CSOAPRequest("readCatalog", "maxidom.soap");
    $request->addParameter("id", intval($catalogId));
    $response = $client->send($request);
    if ( $response->isFault() ) {
        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
//        $brandId = $response->Value["id"];
    }


    echo "createCatalog\n";
    $ap_param = [
        "BRAND_ID" => $brandId,
        "ELEMENT_ID" => 20,
        "SORT" => 100,
    ];
    $request = new CSOAPRequest("createCatalog", "maxidom.soap");
    $request->addParameter("arParams", $ap_param);

//var_dump($request);
    $response = $client->send($request);

    if ( $response->isFault() ) {
        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
        $catalogId = $response->Value["id"];
    }

    echo "\nreadCatalog\n";
    $request = new CSOAPRequest("readCatalog", "maxidom.soap");
    $request->addParameter("id", intval($catalogId));
    $response = $client->send($request);
    if ( $response->isFault() ) {
        echo( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
    }
    else {
        echo( "[OK]: ".print_r($response->Value, 1));
//        $brandId = $response->Value["id"];
    }


}

echo "</pre>";
<?

use Bitrix\Main\Loader;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

Loader::includeModule('belyaev.maxidom');

$APPLICATION->IncludeComponent("bitrix:webservice.server",".default", [
    "WEBSERVICE_NAME" => "maxidom.soap",
    "WEBSERVICE_MODULE" => "belyaev.maxidom",
    "WEBSERVICE_CLASS" => '\Belyaev\Maxidom\soap',
]);
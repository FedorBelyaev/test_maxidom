<?php use Belyaev\Maxidom\BrandTable;
use Belyaev\Maxidom\CatalogTable;
use Bitrix\Main\Loader;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class CBelyaevBrandList extends CBitrixComponent
{
    public function executeComponent()
    {
        if(!Loader::includeModule('belyaev.maxidom'))
            die("module belyaev.maxidom is not loaded");

        $this->arResult["BRAND"] = false;
        $this->arResult["ITEMS"] = [];
        $this->arResult["ELEMENT_IDS"] = [];

        $rsBrand = BrandTable::getList([
            "order" => ["SORT" => "ASC"],
            "select" => ["*"],
            "filter" => ["BRAND_ID" => $this->request->get("BRAND_ID")],
        ]);
        if($rowBrand = $rsBrand->fetch()){
            // вытащим бренд
            $this->arResult["BRAND"] = $rowBrand;
        }

        if($this->arResult["BRAND"]){
            // вытащим список элементов бренда
            $rsCatalog= CatalogTable::getList([
                "order" => ["SORT" => "ASC"],
                "select" => ["*"],
                "filter" => ["BRAND_ID" => $this->arResult["BRAND"]["ID"]],
            ]);
            while($rowCatalog = $rsCatalog->fetch()){
                $this->arResult["ITEMS"][] = $rowCatalog;
                $this->arResult["ELEMENT_IDS"][]  = $rowCatalog["ELEMENT_ID"];
            }
        }

        $this->includeComponentTemplate();
    }

    public function onPrepareComponentParams($arParams)
    {
        if(!$arParams["IBLOCK_ID"]){
            $arParams["IBLOCK_ID"] = "";
        }
        if(!$arParams["SECTION_URL"]){
            $arParams["SECTION_URL"] = "";
        }
        if(!$arParams["DETAIL_URL"]){
            $arParams["DETAIL_URL"] = "";
        }
        if(!$arParams["BASKET_URL"]){
            $arParams["BASKET_URL"] = "";
        }
        return $arParams;
    }
}

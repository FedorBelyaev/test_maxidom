<?php
$arComponentParameters = [
    "PARAMETERS" => [
        "IBLOCK_ID" => array(
            "NAME" => "IBLOCK_ID",
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "",
        ),
        "DETAIL_URL" => array(
            "NAME" => "DETAIL_URL",
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "",
        ),
        "SECTION_URL" => array(
            "NAME" => "SECTION_URL",
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "",
        ),
        "BASKET_URL" => array(
            "NAME" => "BASKET_URL",
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "",
        ),
    ],
];
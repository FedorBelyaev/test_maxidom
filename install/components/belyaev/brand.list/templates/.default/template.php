<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $arFilterHL;
$arFilterHL = [
    "ID" => $arResult["BRAND_IDS"],
];
$APPLICATION->IncludeComponent("bitrix:highloadblock.list", '', [
    "BLOCK_ID" => 2,
    "FILTER_NAME" => "arFilterHL",
    "DETAIL_URL" => "/maxidom_brand/catalog.php?BRAND_ID=#ID#"
]);

<?php use Belyaev\Maxidom\BrandTable;
use Bitrix\Main\Loader;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class CBelyaevBrandList extends CBitrixComponent
{
    public function executeComponent()
    {
        if(!Loader::includeModule('belyaev.maxidom'))
            die("module belyaev.maxidom is not loaded");

        $arElement = [];
        $arBrandIds = [];
        $rs = BrandTable::getList([
            "order" => ["SORT" => "ASC"],
            "select" => ["ID", "BRAND_ID"],
        ]);
        while($row = $rs->fetch()){
            $arElement[] =  $row;
            $arBrandIds[] =  $row["BRAND_ID"];
        }
        $this->arResult["ITEMS"] = $arElement;
        $this->arResult["BRAND_IDS"] = $arBrandIds;
        $this->includeComponentTemplate();
    }

    public function onPrepareComponentParams($arParams)
    {
        return $arParams;
    }
}

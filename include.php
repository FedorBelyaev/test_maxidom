<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Loader;
// TODO: данный файл не требуется если модуль не содержит классов
// TODO: при наличии D7 файл не требуется, из каталога lib все классы подключаются автоматически
Loader::registerAutoLoadClasses('belyaev.maxidom', [
    // no thanks, bitrix, we better will use psr-4 than your class names convention
    '\Belyaev\Maxidom\BrandTable' => 'lib/brandtable.php',
    '\Belyaev\Maxidom\CatalogTable' => 'lib/catalogtable.php',
    '\Belyaev\Maxidom\Events' => 'lib/events.php',
    '\Belyaev\Maxidom\Soap' => 'lib/soap.php',
]);



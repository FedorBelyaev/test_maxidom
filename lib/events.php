<?php
namespace Belyaev\Maxidom;

use Bitrix\Main\Event;
use Bitrix\Main\Loader;
use CSOAPClient;
use CSOAPRequest;

class Events
{
    public static function OnSaleOrderSaved(Event $event)
    {
        $ENTITY = $event->getParameter("ENTITY");
        $VALUES = $event->getParameter("VALUES");
        $IS_NEW = $event->getParameter("IS_NEW");
//        error_log(__METHOD__.': $ENTITY: '.var_export($ENTITY,true));
//        error_log(__METHOD__.': $VALUES: '.var_export($VALUES,true));
//        error_log(__METHOD__.': $IS_NEW: '.var_export($IS_NEW,true));

        if($IS_NEW){
            // отправляем Order через SOAP
            if(Loader::includeModule("webservice")){
                $client  = new CSOAPClient($_SERVER["SERVER_NAME"], '/test_soap.php');
                $request = new CSOAPRequest("testSendOrder", "maxidom.soap");
                $request->addParameter("arParams",[
                    "ENTITY" => $ENTITY,
                    "VALUES" => $VALUES,
                    "IS_NEW" => $IS_NEW,
                ]);
                $response = $client->send($request);
                if($response){
                    if ( $response->isFault() ) {
                        error_log( "SOAP fault: " . $response->faultCode(). " - " . $response->faultString() . "" );
                    }
                    else {
                        error_log( "[OK]: ".print_r($response->Value, 1));
                    }

                }
            }
        }
    }
}
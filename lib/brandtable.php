<?php
namespace Belyaev\Maxidom;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\Relations\OneToMany;
use Bitrix\Main\ORM\Fields\StringField;

Loc::loadMessages(__FILE__);

class BrandTable extends DataManager
{
    public static function getTableName()
    {
        return 'belyaev_maxidom_brand';
    }

    public static function getMap()
    {
        return [
            new IntegerField('ID', [
                'autocomplete' => true,
                'primary' => true,
                'title' => Loc::getMessage('BELYAEV_MAXIDOM_BRAND_ID'),
            ]),
//            new StringField('NAME', [
//                'required' => true,
//                'title' => Loc::getMessage('BELYAEV_MAXIDOM_BRAND_NAME'),
//                'default_value' => "",
////                'validation' => function () {
////                    return [
////                        new Validator\Length(null, 255),
////                    ];
////                },
//            ]),
            new IntegerField('SORT', [
                'required' => false,
                'title' => Loc::getMessage('BELYAEV_MAXIDOM_CATALOG_SORT'),
                'default' => 500,
            ]),
            (new OneToMany('CATALOG', CatalogTable::class, 'BRAND'))->configureJoinType('inner'),
            new IntegerField('BRAND_ID', [
                'required' => true,
                'title' => Loc::getMessage('BELYAEV_MAXIDOM_BRAND_BRAND_ID'),
            ]),
        ];
    }
}

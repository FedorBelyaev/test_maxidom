<?php
namespace Belyaev\Maxidom;


use Bitrix\Main\Application;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use CWebService;
use CWebServiceDesc;
use Exception;
use IWebService;

/**
 * Class для удобных методов SOAP
 * @package Belyaev\Tasks
 */
class soap extends IWebService
{
    /**
     *
     */
    function GetWebServiceDesc()
    {
        $wsdesc = new CWebServiceDesc();

        $wsdesc->wsname = "maxidom.soap";
        $wsdesc->wsclassname = "Soap";
        $wsdesc->wsdlauto = true;
        /** @noinspection PhpDynamicAsStaticMethodCallInspection */
        $wsdesc->wsendpoint = CWebService::GetDefaultEndpoint();
        /** @noinspection PhpDynamicAsStaticMethodCallInspection */
        $wsdesc->wstargetns = CWebService::GetDefaultTargetNS();
        $wsdesc->classTypes = [];
        $wsdesc->structTypes = [
            "BrandNew" => [
                //"NAME" => ["varType" => "string"],
                "BRAND_ID" => ["varType" => "integer"],
                "SORT" => ["varType" => "integer", "strict" => "no"],
            ],
            "BrandUpdate" => [
                //"NAME" => ["varType" => "string"],
                "BRAND_ID" => ["varType" => "integer", "strict" => "no"],
                "SORT" => ["varType" => "integer", "strict" => "no"],
            ],
            "Brand" => [
                "ID" => ["varType" => "integer"],
                "SORT" => ["varType" => "integer"],
                "BRAND_ID" => ["varType" => "integer"],
            ],
            "CatalogNew" => [
                "BRAND_ID" => ["varType" => "integer"],
                "ELEMENT_ID" => ["varType" => "integer"],
                "SORT" => ["varType" => "integer", "strict" => "no"],
            ],
            "CatalogUpdate" => [
                "BRAND_ID" => ["varType" => "integer", "strict" => "no"],
                "ELEMENT_ID" => ["varType" => "integer", "strict" => "no"],
                "SORT" => ["varType" => "integer", "strict" => "no"],
            ],
            "Catalog" => [
                "ID" => ["varType" => "integer"],
                "BRAND_ID" => ["varType" => "integer"],
                "SORT" => ["varType" => "integer" ],
                "ELEMENT_ID" => ["varType" => "integer"],
            ],
            "Order" => [
                "ENTITY" => ["varType" => "any" , "strict" => "no"],
                "VALUES" => ["varType" => "any" , "strict" => "no"],
                "IS_NEW" => ["varType" => "boolean" , "strict" => "no"],
            ],
        ];

        $wsdesc->classes = [
            '\Belyaev\Maxidom\Soap' => [
                "createBrand" => [
                    "type"		=> "public",
                    "name"		=> "createBrand",
                    "input"		=> [
                        "arParams" => ["varType" => "BrandNew", "strict" => "no"],
                    ],
                    "output"	=> [
                        "id" => ["varType" => "integer", "strict" => "no"],
                        "success" => ["varType" => "boolean", "strict" => "no"],
                    ],

                ],
                "readBrand" => [
                    "type"		=> "public",
                    "name"		=> "readBrand",
                    "input"		=> [
                        "id" => ["varType" => "integer", "strict" => "no"],
                    ],
                    "output"	=> [
                        "brand" => ["varType" => "Brand", "strict" => "no"],
                        "success" => ["varType" => "boolean", "strict" => "no"],
                    ],
                ],
                "updateBrand" => [
                    "type"		=> "public",
                    "name"		=> "updateBrand",
                    "input"		=> [
                        "id" => ["varType" => "integer", "strict" => "no"],
                        "arParams" => ["varType" => "BrandUpdate", "strict" => "no"],
                    ],
                    "output"	=> [
                        "success" => ["varType" => "boolean", "strict" => "no"],
                    ],

                ],
                "deleteBrand" => [
                    "type"		=> "public",
                    "name"		=> "deleteBrand",
                    "input"		=> [
                        "id" => ["varType" => "integer", "strict" => "no"],
                    ],
                    "output"	=> [
                        "success" => ["varType" => "boolean", "strict" => "no"],
                    ],
                ],
                "createCatalog" => [
                    "type"		=> "public",
                    "name"		=> "createCatalog",
                    "input"		=> [
                        "arParams" => ["varType" => "CatalogNew", "strict" => "no"],
                    ],
                    "output"	=> [
                        "id" => ["varType" => "integer", "strict" => "no"],
                        "success" => ["varType" => "boolean", "strict" => "no"],
                    ],

                ],
                "readCatalog" => [
                    "type"		=> "public",
                    "name"		=> "readCatalog",
                    "input"		=> [
                        "id" => ["varType" => "integer", "strict" => "no"],
                    ],
                    "output"	=> [
                        "catalog" => ["varType" => "Catalog", "strict" => "no"],
                        "success" => ["varType" => "boolean", "strict" => "no"],
                    ],
                ],
                "updateCatalog" => [
                    "type"		=> "public",
                    "name"		=> "updateCatalog",
                    "input"		=> [
                        "id" => ["varType" => "integer", "strict" => "no"],
                        "arParams" => ["varType" => "CatalogUpdate", "strict" => "no"],
                    ],
                    "output"	=> [
                        "success" => ["varType" => "boolean", "strict" => "no"],
                    ],

                ],
                "deleteCatalog" => [
                    "type"		=> "public",
                    "name"		=> "deleteCatalog",
                    "input"		=> [
                        "id" => ["varType" => "integer", "strict" => "no"],
                    ],
                    "output"	=> [
                        "success" => ["varType" => "boolean", "strict" => "no"],
                    ],
                ],
                "testSendOrder" => [
                    "type"		=> "public",
                    "name"		=> "testSendOrder",
                    "input"		=> [
                        "arParams" => ["varType" => "Order", "strict" => "no"],
                    ],
                    "output"	=> [
                        "success" => ["varType" => "boolean", "strict" => "no"],
                    ],
                ],
            ],
        ];
        return $wsdesc;
    }

    /** @noinspection PhpUnused */
    /**
     * @param $arParams
     * @return array|bool|int
     * @throws Exception
     */
    public function createBrand($arParams)
    {
//        if(!is_array($arParams) && is_string($arParams)){
//            $arParams = json_decode($arParams, true);
//        }
        error_log(__METHOD__.': $arParams: '.var_export($arParams,true));


        $result = BrandTable::add($arParams);
        if ($result->isSuccess())
        {
            error_log('$result->getId(): '.var_export($result->getId(),true));
            return ["id" => $result->getId(), "success" => true];
        }
        return ["id" => 0, "success" => false];
    }

    /** @noinspection PhpUnused */
    /**
     * @param $id
     * @return array|bool|false
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function readBrand($id)
    {
        $result = BrandTable::getById($id);
        /** @noinspection PhpUndefinedMethodInspection */
        if ($row = $result->fetch())
        {
            return ["brand" => $row, "success" => true];

        }
        return ["brand" => false, "success" => false];
    }

    /** @noinspection PhpUnused */
    /**
     * @param $id
     * @param $arParams
     * @return array
     * @throws Exception
     */
    public function updateBrand($id, $arParams)
    {
        if(!is_array_assoc($arParams) && is_string($arParams)){
            $arParams = json_decode($arParams, true);
        }

        $result = BrandTable::update($id, $arParams);
        if ($result->isSuccess())
        {
            return ["success" => true];
        }
        return ["success" => false];
    }

    /** @noinspection PhpUnused */
    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function deleteBrand($id)
    {
        $result = BrandTable::delete($id);
        if ($result->isSuccess())
        {
            return ["success" => true];
        }
        return ["success" => false];
    }


    /** @noinspection PhpUnused */
    /**
     * @param $arParams
     * @return array|bool|int
     * @throws Exception
     */
    public function createCatalog($arParams)
    {
        $result = CatalogTable::add($arParams);
        if ($result->isSuccess())
        {
            return ["id" => $result->getId(), "success" => true];
        }
        return ["id" => 0, "success" => false];
    }

    /** @noinspection PhpUnused */
    /**
     * @param $id
     * @return array|bool|false
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function readCatalog($id)
    {
        $result = CatalogTable::getById($id);
        /** @noinspection PhpUndefinedMethodInspection */
        if ($result->isSuccess())
        {
            return ["catalog" => $result->fetch(), "success" => true];
        }
        return ["catalog" => false, "success" => false];
    }

    /** @noinspection PhpUnused */
    /**
     * @param $id
     * @param $arParams
     * @return bool
     * @throws Exception
     */
    public function updateCatalog($id, $arParams)
    {
        if(!is_array_assoc($arParams) && is_string($arParams)){
            $arParams = json_decode($arParams, true);
        }

        $result = CatalogTable::update($id, $arParams);
        if ($result->isSuccess())
        {
            return ["id" => $result->getId(), "success" => true];
        }
        return ["id" => 0, "success" => false];
    }

    /** @noinspection PhpUnused */
    /**
     * @param $id
     * @return bool
     * @throws Exception
     */
    public function deleteCatalog($id)
    {
        $result = CatalogTable::delete($id);
        if ($result->isSuccess())
        {
            return ["success" => true];
        }
        return ["success" => false];
    }

    /**
     * @param $arParams
     * @return array
     */
    public function testSendOrder($arParams)
    {
        try{
            file_put_contents(Application::getDocumentRoot().'/test_soap.php.txt', '$arParams: '.var_export($arParams, true));
        }
        catch (Exception $e){
            return ["success" => false];
        }
        return ["success" => true];
    }

}
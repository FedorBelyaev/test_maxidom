<?php
namespace Belyaev\Maxidom;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\Relations\OneToMany;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\ORM\Query\Join;

Loc::loadMessages(__FILE__);

class CatalogTable extends DataManager
{
    public static function getTableName()
    {
        return 'belyaev_maxidom_catalog';
    }

    public static function getMap()
    {
        return [
            new IntegerField('ID', [
                'autocomplete' => true,
                'primary' => true,
                'title' => Loc::getMessage('BELYAEV_MAXIDOM_CATALOG_ID'),
            ]),
            new IntegerField('BRAND_ID', [
                'required' => true,
                'title' => Loc::getMessage('BELYAEV_MAXIDOM_CATALOG_BRAND_ID'),
            ]),
            new IntegerField('SORT', [
                'required' => false,
                'title' => Loc::getMessage('BELYAEV_MAXIDOM_CATALOG_SORT'),
                'default' => 500,
            ]),
            (new Reference(
                'BRAND',
                BrandTable::class,
                Join::on('this.BRAND_ID', 'ref.ID')
            ))->configureJoinType('inner'),

            new IntegerField('ELEMENT_ID', [
                'required' => true,
                'title' => Loc::getMessage('BELYAEV_MAXIDOM_CATALOG_ELEMENT_ID'),
            ]),
        ];
    }
}

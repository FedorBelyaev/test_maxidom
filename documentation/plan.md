### а. структура данных, варианты:

а.1. структуру данных сформировать в инфоблоке

### б. компонент обертка для портирования параметров структуры данных в стандартные компоненты catalog.filter, catalog.section, catalog.element

### в. компонент корзины (вероятно, на основе стандартного компонента)

### г. SOAP endpoint, для вызова функция битрикс
г.1. реализовать компонентом битрикс bitrix:webservice.server

### д. код планирую организовать в виде модуля

### е. структура тестирования
```
acceptance
    Frontend
        BrandListCest.php
        CatalogListCest.php
        CatalogListElementCest.php
        CatalogListFilterCest.php
        OrderCest.php
    Module
        InstallModuleCest.php
        SoapCrudCest.php
        UninstallModuleCest.php

api
    CreateBrandCest.php
    CreateCatalogCest.php
    DeleteBrandCest.php
    DeleteCatalogCest.php
    ReadBrandCest.php
    ReadCatalogCest.php
    UpdateBrandCest.php
    UpdateCatalogCest.php
```